using System;

namespace InterfaceSplitter{

  public interface ISplitter{
    public void addLeftInputNameAndProDecamill();
    public void addRightInputNameAndProDecamill();
    private Product createAProduct();
    public Product[] getLeftInputs();
    public Product[] getRightInputs();
    public void calculateOutput();
    private void compileInputs();
    private int compareIndexWithRightInput();
    private Product consolidateInput();
    private bool lookupInput();
    public string getOutputString();
    private string generateOutputString();
    public Product[] getOutputs();
    public bool compareOutputs();
  }
}
