using System;

namespace balancer
{

  public class Product
  {
    private string Name;
    private int ProDecamill;

    public void setName(string name)
    {
      this.Name = name;
      return;
    }

    public void setProDecamill(int decamill)
    {
      this.ProDecamill = decamill;
      return;
    }

    public void setNameAndProDecamill(string name, int decamill)
    {
      setName(name);
      setProDecamill(decamill);
      return;
    }

    public string getName(){return this.Name;}

    public int getProDecamill(){return this.ProDecamill;}
  }
}
