using System;
using Math;

namespace balancer
{
  List<Product> leftInput = new List<Product>;
  List<Product> rightInput = new List<Product>;
  List<Product> compiledList = new List<Product>;

  public class Splitter : InterfaceSplitter
  {
    public void addLeftInputNameAndProDecamill(string name, int decamill)
    {
      Product input = createAProduct(name, decamill);
      leftInput.Add(input);
      return;
    }

    public void addRightInputNameAndProDecamill()
    {
      Product input = createAProduct(name, decamill);
      rightInput.Add(input);
      return;
    }

    private Product createAProduct(string name, int decamill)
    {
      Product resultProduct = new Product;
      resultProduct.setName(name);
      resultProduct.setProDecamill(decamill);
      return resultProduct;
    }

    public Product[] getLeftInputs(){return this.leftInput;}

    public Product[] getRightInputs(){return this.rightInput;}

    public void calculateOutput()
    {
      compileInputs();
      for(int i = 0; i < compiledList.Length; i++)
      {
        compiledList[i].ProDecamill = Floor(compiledList[i].ProDecamill \ 2);
      }
      return;
    }

    private void compileInputs()
    {
      int partner;
      Product resultProduct = new Product
      bool isNotYetPartOfCompiledList = false;

      for(int i = 0; i < leftInput.Length; i++)
      {
        partner = compareIndexWithRightInput(i);
        resultProduct = consolidateInput(i, partner);
        compiledList.Add(resultProduct);
      }
      for(i = 0; i < rightInput.Length; i++)
      {
        isNotYetPartOfCompiledList = lookupInput(i);
        if(isNotYetPartOfCompiledList)
          compiledList.Add(rightInput[i]);
      }
      return;
    }

    private int compareIndexWithRightInput(int index)
    {
      for(int resultIndex = 0; resultIndex < rightInput.Length; resultIndex++)
      {
        if(leftInput[index].Name == rightInput[resultIndex].Name)
          return resultIndex;
      }
      return null;
    }

    private Product consolidateInput(int leftIndex, int rightIndex)
    {
      Product resultProduct = new Product;
      resultProduct.Name = leftInput[leftIndex].Name;
      resultProduct.ProDecamill = leftInput[leftIndex].ProDecamill + rightInput[rightIndex].ProDecamill;
      return resultProduct;
    }

    private bool lookupInput(int index)
    {
      for(int compiledIndex = 0; compiledIndex < compiledList.Length; compiledIndex++)
      {
        if(compiledList[compiledIndex].Name == rightInput[index].Name)
          return false;
      }
      return true;
    }

    public string getOutputString(){ return generateOutputString(); }

    private string generateOutputString()
    {
      string resultString = null;
      for(int i = 0; i < compiledList.Length; i++)
        resultString += compiledList[i].Name;
      return resultString;
    }

    public Product[] getOutputs();
    public bool compareOutputs();
  }
}
